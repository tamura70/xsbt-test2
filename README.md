# README

## 実行

1. `sbt ~Tomcat/start`
2. Open <http://localhost:8080>

## APIテスト

- <http://localhost:8080/api?c=date>
- <http://localhost:8080/api?c=draw&n=1>
- <http://localhost:8080/api?c=items>
- <http://localhost:8080/api?c=item&id=001>
- <http://localhost:8080/api?c=item&id=999>

## Heroku

1. `heroku login -i`
2. `heroku create`
3. Edit `build.sbt`
4. `sbt herokuDeploy`

## リンク

- [Apache: Tomcat](http://tomcat.apache.org/)
- [GitHub: xsbt-web-plugin](https://github.com/earldouglas/xsbt-web-plugin)
- [GitHub: json4s](https://github.com/json4s/json4s)
- [GitHub: viz](https://github.com/mdaines/viz.js)
- [GitHub: d3-graphviz](https://github.com/magjac/d3-graphviz)
- [D3.js](https://d3js.org/)

