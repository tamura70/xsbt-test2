function escapeHTML(str) {
    return str.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll('"', "&quot;");
}

function sendRequest(path, callback) {
    var url = window.location.protocol + "//" + window.location.host + path;
    var http = new XMLHttpRequest();
    http.overrideMimeType("text/plain; charset=UTF-8");
    http.open("GET", url, true);
    if (callback) {
	http.onreadystatechange = function () {
            if (http.readyState != 4) {
	        return;
            }
            if (http.status != 200) {
	        alert("Server is not responding : " + http.status);
	        return;
            }
            callback(http);
        };
    }
    http.send(null);
}
